import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static final Logger logger = LogManager.getLogger(Main.class);

    private static final String URL_JOKES_CATEGORIES = "https://api.chucknorris.io/jokes/categories";

    private static final String URL_RANDOM_CATEGORIES = "https://api.chucknorris.io/jokes/random?category={category}";

    private static List<String> categories = new ArrayList<>();

    private static HttpURLConnection httpURLConnection;

    private static URL url;

    public static void main(String[] args) {
        readCategories();
        for (String category : categories) {
            readJokes(category);
        }
        httpURLConnection.disconnect();
    }

    private static void readCategories() {
        String inputLine;
        StringBuilder content = new StringBuilder();
        try {
            url = new URL(URL_JOKES_CATEGORIES);
            httpURLConnection = (HttpURLConnection) url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            while ((inputLine = bufferedReader.readLine()) != null) {
                content.append(inputLine);
            }
            ObjectMapper objectMapper = new ObjectMapper();
            JavaType javaType = objectMapper.getTypeFactory().constructCollectionType(List.class, String.class);
            categories = objectMapper.readValue(content.toString(), javaType);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

    }

    private static void readJokes(String category) {
        String inputLine;
        StringBuilder content = new StringBuilder();
        try {
            URL url = new URL(URL_RANDOM_CATEGORIES.replace("{category}", category));
            httpURLConnection = (HttpURLConnection) url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            while ((inputLine = bufferedReader.readLine()) != null) {
                content.append(inputLine);
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        String result = content.toString();
        String substr = "\"value\"";
        if (result.contains(substr)) {
            result = result.substring(result.indexOf(substr) + substr.length());
            logger.info(category + " " + result);
        }
    }

}
